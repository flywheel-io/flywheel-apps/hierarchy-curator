"""An example curation script to clear metadata at various levels of the hierarchy."""

import logging
from typing import List, Optional

import flywheel
from flywheel_gear_toolkit.utils.curator import HierarchyCurator

######## Set these first ###############
# Do you want to complete the calls (False) or see what the script would do (True)?
DRY_RUN = True
# Set the stop_level to whichever container is the final level that
# needs to be cleared (i.e., project, subject, session, acquisition)
STOP_LEVEL = "subject"
# Do you wish to reset the modality (acquisitions)?
# This will require using file-classifier again since it will
# clear even the basic modality (e.g., MR), not
# just the classifications (e.g., T1w)
CLEAR_MODALITY_BOOL = False
#######################################

# Configure the logger
log = logging.getLogger(__name__)


def clean_top_level(
    container, dry_run: bool = False, reset_keys: Optional[List[str]] = None
):
    """Clear the specified keys that are easily viewable in the UI for a given container.

    Each container has a set of metadata that are automatically visible in the UI.
    This method should be used to reset unprotected fields (e.g., cohort), but not
    label or other 'hidden' metadata fields.

    Args:
        container: container object of any of the Flywheel container types
        dry_run (Boolean): The commands will be echoed, rather than executed.
        reset_keys (List, optional): safe keys available from container.update(), which
                            should be reset by the gear to None
    """
    log.info("Updating %s", container.label)

    for k in reset_keys:
        if k in container.keys():
            if dry_run:
                log.info("Would run container.update(%s: None)", k)
            else:
                container.update({k: None})
        else:
            log.info("%s did not match available keys:\n%s", k, container.keys())


def clean_secondary_level(container, dry_run: bool = False):
    """Reset the metadata on a specific container, including a single file.

    Args:
        container: container object of any of the Flywheel container types
        dry_run (Boolean): log what would happen versus actually doing it.
    """
    # Define metadata resetting methods to try/use
    reload_method = getattr(container, "reload", None)
    # e.g., replace_classification:{} will become file.replace_classification({}),
    # which replaces any existing classifications with an empty dictionary.
    replace_classification_method = getattr(container, "replace_classification", None)
    replace_info_method = getattr(container, "replace_info", None)

    if reload_method:
        if dry_run:
            log.info("\tWould execute reload()")
        else:
            reload_method()

    if replace_classification_method:
        if dry_run:
            log.info("\tWould execute replace_classification({})")
        else:
            replace_classification_method({})

    if replace_info_method:
        if dry_run:
            log.info("\tWould execute replace_info({})")
        else:
            replace_info_method({})

    # Search for and clear any tags separately
    for t in list(container.tags):
        if dry_run:
            log.info("\tWould call container.delete_tag(%s)", t)
        else:
            container.delete_tag(t)


def clear_modality(container, dry_run: bool = True):
    """Reset the modality field on a file container.

    Args:
        container: container object of any of the Flywheel container types
        dry_run (Boolean): log what would happen versus actually doing it.
    """
    if dry_run:
        log.info("\tWould run container.update(modality=None)")
    else:
        container.update(modality=None)


class Curator(HierarchyCurator):
    """A curation script to clear metadata on based on a given container type.

    For each container type that has a child container, the curator will descend
    into the child container(s) and perform the metadata clearing. The container,
    itself, will have its metadata cleared after the child containers are cleared.
    """

    def __init__(self, *args, **kwargs):
        """Initialize the curator."""
        # Pass clear_modality explicitly if needed by the parent class
        super().__init__(*args, **kwargs)
        self.clear_modality = CLEAR_MODALITY_BOOL
        self.config.stop_level = STOP_LEVEL.lower()
        self.dry_run = DRY_RUN

    def curate_acquisition(self, acquisition: flywheel.Acquisition):
        """Curate an acquisition.

        Reset the classifications, custom info, tags, and (optionally) modality
        for an acquisition and associated files.
        """
        log.info("Curating acquisition: %s", acquisition.label)
        for file_ in acquisition.files:
            log.info(" Curating file: %s", file_.name)
            log.debug("\tCurrent classification: %s", file_.classification)
            clean_secondary_level(file_, self.dry_run)
            if self.clear_modality:
                clear_modality(file_, self.dry_run)
        log.info("Clearing acquisition metadata: %s", acquisition.label)
        clean_secondary_level(acquisition, self.dry_run)

    def curate_session(
        self, sess: flywheel.Session, reset_keys: Optional[List[str]] = None
    ):
        """Curate a session.

        Reset metadata viewable in the UI and any custom info or tags
        for a session.

        Args:
            sess: The session container being curated.
            reset_keys (List, optional): safe keys available from container.update(), which should be reset by the gear to None
        """
        if not reset_keys:
            # use default list of safe keys to reset on a session
            reset_keys = ["age", "weight", "operator"]

        log.info("Clearing session metadata: %s", sess.label)
        clean_top_level(sess, self.dry_run, reset_keys)
        clean_secondary_level(sess, self.dry_run)

    def curate_subject(
        self, subj: flywheel.Subject, reset_keys: Optional[List[str]] = None
    ):
        """Curate a subject.

        Reset metadata viewable in the UI and any custom info or tags
        for a subject.

        Args:
            subj: The subject container being curated.
            reset_keys (List, optional): safe keys available from container.update(), which should be reset by the gear to None
        """
        if not reset_keys:
            # use default list of safe keys to reset on a subject
            reset_keys = [
                "cohort",
                "date_of_birth",
                "ethnicity",
                "firstname",
                "lastname",
                "mlset",
                "race",
                "sex",
                "state",
                "strain",
            ]

        log.info("Clearing subj metadata: %s", subj.label)
        clean_top_level(subj, self.dry_run, reset_keys)
        clean_secondary_level(subj, self.dry_run)

    def curate_project(self, proj: flywheel.Project):
        """Curate a project.

        Reset custom info and tags, but not attachments, description, etc.
        for a given project.


        Args:
            proj: The project container being curated.
        """
        log.info("Curating project %s", proj.label)
        clean_secondary_level(proj, self.dry_run)
