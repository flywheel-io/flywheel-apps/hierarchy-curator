from unittest.mock import MagicMock

import flywheel
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_hierarchy_curator.parser import parse_config


def test_parse_config():
    gear_context = MagicMock(spec=GearToolkitContext)
    gear_context.destination = {"id": "test"}
    # Create a mock for config_json and configure its behavior
    config_json_mock = MagicMock()
    config_json_mock.config = {"reload-flywheel-sdk": False}

    gear_context.config_json = config_json_mock

    gear_context.client.get_analysis.return_value = flywheel.AnalysisOutput(
        parent=flywheel.ContainerReference(type="subject", id="test12")
    )

    parse_config(gear_context)

    gear_context.client.get_analysis.assert_called_once_with("test")
    gear_context.client.get_subject.assert_called_once_with("test12")
    gear_context.get_input_path.called_count == 5
    assert gear_context.config_json.config["reload-flywheel-sdk"] is False
